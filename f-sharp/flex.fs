#light

let help =
    "Flex v1.0\n\n" +
    "Uso:\n" +
    "  flex.exe <preço da gasolina> <preço do álcool>\n\n" + 
    "Exemplo:\n" +
    "  flex.exe 3.69 2.99\n"

let flex gasolina alcool =
    let perc        = System.Double.Parse(alcool) * 100.0 / System.Double.Parse(gasolina)
    let combustivel = if perc >= 70.0 then "gasolina" else "álcool"
    "Abasteça com " + combustivel + "."

let is_numeric a = fst (System.Double.TryParse(a))

[<EntryPoint>]
let main args =
    if args.Length = 2 && is_numeric(args.[0]) && is_numeric(args.[1]) then
        let result   = flex args.[0] args.[1]
        printfn "%s" result
    else
        //help
        printfn "%s" help
    0
