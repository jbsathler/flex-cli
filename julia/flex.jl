#!/usr/bin/env julia

function help()
	"Flex v1.0\n\n" *
		"Uso:\n" *
		"  flex.fl <preço da gasolina> <preço do álcool>\n\n" *
		"Exemplo:\n" *
		"  flex.fl 3.69 2.99\n"
end

function flex(gasolina, alcool)
	perc = alcool * 100 / gasolina
	combustivel = (perc >= 70 ? "gasolina" : "álcool")
	"Abasteça com " * combustivel * "."
end

function isNumeric{T<:AbstractString}(s::T)
    isa(parse(s), Number)
end

if length(ARGS) == 2 && isNumeric(ARGS[1]) && isNumeric(ARGS[2])
	println(flex(parse(ARGS[1]), parse(ARGS[2])))
else
	println(help())
end
