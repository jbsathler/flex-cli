#!/usr/bin/env lua

function help()
	return "Flex v1.0\n\n" ..
		"Uso:\n" ..
		"  flex.lua <preço da gasolina> <preço do álcool>\n\n" ..
		"Exemplo:\n" ..
		"  flex.lua 3.69 2.99\n"
end

function flex (gasolina, alcool)
	perc = alcool * 100 / gasolina
	combustivel = (perc >= 70 and "gasolina" or "álcool")
	return ("Abasteça com: " .. combustivel .. ".")
end

if #arg == 2 and tonumber(arg[1]) ~= nil and tonumber(arg[2]) ~= nil then
	print(flex(tonumber(arg[1]), tonumber(arg[2])))
else
	print(help())
end
