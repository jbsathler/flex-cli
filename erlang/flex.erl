%% coding: utf-8
-module(flex).
-export([main/1, main/0]).

help() ->
	"Flex v1.0~n~n" ++
	"Uso:~n" ++
	"  erl -noshell -s flex main <preco da gasolina> <preco do alcool> -s init stop~n~n" ++
	"Exemplo:~n" ++
	"  erl -noshell -s flex main 3.69 2.99 -s init stop~n~n".

flex(Gasolina, Alcool) ->
	Perc = Alcool * 100 / Gasolina,
	Combustivel = case Perc >= 70 of true -> "gasolina"; false -> "alcool" end,
	"Abasteca com " ++ [Combustivel] ++ ".~n".

is_numeric(L) ->
	Float = (catch atom_to_float(L)),
	Int = (catch atom_to_integer(L)),
	is_number(Float) orelse is_number(Int).

atom_to_float(A) ->
	erlang:list_to_float(erlang:atom_to_list(A)).

atom_to_integer(A) ->
	erlang:list_to_integer(erlang:atom_to_list(A)).

main(Args) ->
	io:fwrite(
		case length(Args) == 2 of
			true ->
				[Gasolina, Alcool] = Args,
				case is_numeric(Gasolina) and is_numeric(Alcool) of
					true ->
						flex(atom_to_float(Gasolina), atom_to_float(Alcool));
					false ->
						help()
				end;
			false ->
				help()
		end
	).
main() -> io:fwrite(help()).
