PROCEDURE help()
	? "Flex v1.0"
	? " "
	? "Uso:"
	? "  flex <pre�o da gasolina> <pre�o do �lcool>"
	? " "
	? "Exemplo:"
	? "  flex 3.69 2.99"
	? " "
RETURN

PROCEDURE flex(gasolina, alcool)
	perc := (alcool * 100 / gasolina)
	combustivel := IF(perc >= 70, "gasolina", "�lcool")
	? "Abaste�a com " + combustivel + "."
RETURN

FUNCTION is_numeric( value )
	LOCAL res
	IF value == NIL
		res := .F.
	ELSEIF Val(value) == 0
		res := .F.
	ELSE
		res := .T.
	ENDIF
RETURN res

PROCEDURE Main(gasolina, alcool)
	IF is_numeric(gasolina) .AND. is_numeric(alcool)
		flex(Val(gasolina), Val(alcool))
	ELSE
		help()
	ENDIF
RETURN
