Red [Title: "Flex v1.0"]

help: rejoin [
  "Flex v1.0" newline
  newline
  "Uso:" newline
  "  flex <preço da gasolina> <preço do álcool>" newline
  newline
  "Exemplo:" newline
  "  flex 3.69 2.99" newline
]

calcPerc: function [gasolina alcool] [alcool * 100.0 / gasolina]

getCombustivel: function [perc] [either perc >= 70.0 ["gasolina"] ["álcool"]]

flex: function [gasolina alcool] [
  perc: calcPerc gasolina alcool
  combustivel: getCombustivel perc
  rejoin ["Abasteça com " combustivel "."]
]

gasolina: attempt [to float! system/options/args/1]
alcool: attempt [to float! system/options/args/2]

either all [system/options/args/1 <> none system/options/args/2 <> none] [
  print flex gasolina alcool
] [
  print help
]
