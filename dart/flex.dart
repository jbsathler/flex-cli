#!/usr/bin/env dart
import 'dart:io';
import 'dart:convert';
import 'dart:async';

import 'package:args/args.dart';

String help() {
	return "Flex v1.0\n\n" +
		"Uso:\n" +
		"  dart flex.dart <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  dart flex.dart 3.69 2.99\n";
}

String flex(double gasolina, double alcool) {
	double  perc = alcool * 100 / gasolina;
	String combustivel = (perc >= 70) ? "gasolina" : "álcool";
	return "Abasteça com " + combustivel + ".";
}

bool isNumeric(str){
	return (double.parse(str, (e)=> null) != null);
}

void main(List<String> arguments) {
	if ((arguments.length == 2) && isNumeric(arguments[0]) && isNumeric(arguments[1])) {
		print(flex(double.parse(arguments[0]), double.parse(arguments[1])));
	} else {
		print(help());
	}
}
