#!/usr/bin/tclsh

proc help {} {
	puts "Flex v1.0\n"
	puts "Uso:"
	puts "  flex.tcl <preço da gasolina> <preço do álcool>\n"
	puts "Exemplo:"
	puts "  flex.tcl 3.69 2.99\n"
}

proc flex {gasolina alcool} {
	set perc [expr $alcool * 100 / $gasolina]
	set combustivel [expr {$perc >= 70 ? "gasolina" : "álcool"}]
	return "Abasteça com $combustivel."
}

proc is_numeric {var} {
	return [regexp {^[-+]?([0-9]*\.[0-9]+|[0-9]+)$} $var]
}

if {$argc == 2 && [is_numeric [lindex $argv 0]] && [is_numeric [lindex $argv 1]]} {
	set gasolina [format %g [lindex $argv 0]]
	set alcool   [format %g [lindex $argv 1]]
	puts [flex $gasolina $alcool]
} else {
	help
}
