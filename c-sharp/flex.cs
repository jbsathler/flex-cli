using System;

public class Flex {

	protected static String help () {
		return "Flex v1.0\n\n" +
			"Uso:\n" +
			"  flex.exe <preço da gasolina> <preço do álcool>\n\n" +
			"Exemplo:\n" +
			"  flex.exe 3.69 2.99\n";
	}

	protected static string flex (float gasolina, float alcool) {
		float perc = alcool * 100 / gasolina;
		string combustivel = (perc >= 70) ? "gasolina" : "álcool";
		return "Abasteça com: " + combustivel + ".";
	}

	protected static bool isNumeric(string str) {
		float Result;
		return float.TryParse(str, out Result);
	}

	public static void Main(string[] args) {
		if (args.Length == 2 && isNumeric(args[0]) && isNumeric(args[1])) {
			System.Console.WriteLine(flex(float.Parse(args[0]), float.Parse(args[1])));
		} else {
			System.Console.WriteLine(help());
		}
	}

}
