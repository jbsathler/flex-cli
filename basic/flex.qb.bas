IF ((_COMMANDCOUNT = 2) AND (isnum(COMMAND$(1)) <> 0 AND isnum(COMMAND$(2)) <> 0)) THEN
    gasolina! = VAL(COMMAND$(1))
    alcool! = VAL(COMMAND$(2))
    CALL flex(gasolina!, alcool!)
ELSE
    CALL help
END IF

SUB help
    PRINT "Flex v1.0"
    PRINT ""
    PRINT "Uso:"
    PRINT "  flex <pre�o da gasolina> <pre�o do �lcool>"
    PRINT ""
    PRINT "Exemplo:"
    PRINT "  flex 3.69 2.99"
END SUB

SUB flex (gasolina!, alcool!)
    perc! = (alcool! * 100 / gasolina!)
    IF (perc! >= 70) THEN
        combustivel$ = "gasolina"
    ELSE
        combustivel$ = "�lcool"
    END IF
    PRINT "Abaste�a com "; combustivel$; "."
END SUB

FUNCTION isnum (S$)
    T1 = VAL(S$): T1$ = STR$(T1)
    isnum = (T1$ = S$ OR T1$ = " " + S$)
END FUNCTION
