sub help ()
	print "Flex v1.0"
	print ""
	print "Uso:"
	print "  flex <preço da gasolina> <preço do álcool>"
	print ""
	print "Exemplo:"
	print "  flex 3.69 2.99"
	print ""
end sub

sub flex (gasolina as single, alcool as single)
	dim perc as single
	dim combustivel as string
	perc = (alcool * 100 / gasolina)
	combustivel = iif(perc >= 70, "gasolina", "álcool")
	print "Abasteça com "; combustivel; "."
end sub

function isNumeric (strVal as string) as boolean
	dim tmpSingle as single
	dim tmpString as string
	tmpSingle = val(strVal): tmpString = str$(tmpSingle)
	return (tmpString = strVal or tmpString = " " + strVal)
end function

if ((__fb_argc__ = 3) andAlso (isNumeric(command$(1)) and isNumeric(command$(2)))) then
	flex(val(command$(1)), val(command$(2)))
else
	help()
end if
