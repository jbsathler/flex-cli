#!/usr/bin/env powershell
function help {
	echo "Flex v1.0"
	echo ""
	echo "Uso:"
	echo "  flex.cmd <preço da gasolina> <preço do álcool>"
	echo ""
	echo "Exemplo:"
	echo "  flex.cmd 3.69 2.99"
	echo ""
}

function flex {
	Param ([float] $gasolina, [float] $alcool)
	[float] $perc = $alcool * 100 / $gasolina
	if($perc -ge 70){
		[string] $combustivel = "gasolina"
	} else {
		[string] $combustivel = "álcool"
	}
	return "Abasteça com " + $combustivel + "."
}

function isNumeric($x) {
	try {
		0 + $x | Out-Null
		return $true
	} catch {
		return $false
	}
}

if ($args.length -eq 2 -and $(isNumeric($args[0])) -and $(isNumeric($args[1]))) {
	echo $(flex -gasolina $args[0] -alcool $args[1])
} else {
	help
}
