#!/usr/bin/env powershell
function help {
	Write-Host "Flex v1.0" -ForegroundColor Blue
	Write-Host ""
	Write-Host "Uso:" -ForegroundColor White
	Write-Host "  flex.cmd" -ForegroundColor Green -NoNewLine
	Write-Host " <preço da gasolina>" -ForegroundColor Yellow -NoNewLine
	Write-Host " <preço do álcool>" -ForegroundColor Cyan
	Write-Host ""
	Write-Host "Exemplo:" -ForegroundColor White
	Write-Host "  flex.cmd" -ForegroundColor Green -NoNewLine
	Write-Host " 3.69" -ForegroundColor Yellow -NoNewLine
	Write-Host " 2.99" -ForegroundColor Cyan
	Write-Host ""
}

function flex {
	Param ([float] $gasolina, [float] $alcool)
	[float] $perc = $alcool * 100 / $gasolina
	if($perc -ge 70){
		$combustivel = @{nome = "gasolina"; cor = "Yellow";}
	} else {
		$combustivel = @{nome = "álcool"  ; cor = "Cyan";  }
	}
	Write-Host "Abasteça com " -ForegroundColor White -NoNewLine
	Write-Host $combustivel.nome -ForegroundColor $combustivel.cor -NoNewLine
	Write-Host "." -ForegroundColor White
}

function isNumeric($x) {
	try {
		0 + $x | Out-Null
		return $true
	} catch {
		return $false
	}
}

if ($args.length -eq 2 -and $(isNumeric($args[0])) -and $(isNumeric($args[1]))) {
	echo $(flex -gasolina $args[0] -alcool $args[1])
} else {
	help
}
