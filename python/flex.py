#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

def helpMsg() :
	return (
		"Flex v1.0\n\n" +
		"modo de uso:\n" +
		"  flex.py <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  flex.py 3.69 2.99\n"
	)

def flex(gasolina, alcool) :
	perc = (alcool * 100 / gasolina)
	combustivel = "gasolina" if perc >= 70 else "álcool"
	return "Abasteça com " + combustivel + "."

def isFloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

if len(sys.argv) == 3 and isFloat(sys.argv[1]) and isFloat(sys.argv[2]) :
	print flex(float(sys.argv[1]), float(sys.argv[2]))
else :
	print helpMsg()
