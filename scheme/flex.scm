#!/usr/bin/env -S csi -ss

(define help (lambda ()
  (print "Flex v1.0")
  (newline)
  (print "Uso:")
  (print "  csi -ss flex.scm <preço da gasolina> <preço do álcool>")
  (newline)
  (print "Exemplo:")
  (print "  csi -ss flex.scm 3.69 2.99")
  (newline)
))

(define flex (lambda (gasolina alcool)
  (define combustivel (if (>= (/ (* alcool 100) gasolina) 70) "gasolina" "álcool"))
  (print (string-append "Abasteça com " combustivel "."))
))

(define is-numeric (lambda (string)
  (not (equal? #f (string->number string)))
))

(define (main args)
  (define gasolina (if (equal? 2 (length args)) (car  args) ""))
  (define alcool   (if (equal? 2 (length args)) (cadr args) ""))

  (if (and (is-numeric gasolina) (is-numeric alcool)) 
    (flex (string->number gasolina) (string->number alcool))
    (help)
  )
0)
