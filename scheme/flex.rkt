#!/usr/bin/env racket
#lang racket/base

(define help (lambda ()
  (display (string-append
    "Flex v1.0\n\n"
    "Uso:\n"
    "  racket flex.rkt <preço da gasolina> <preço do álcool>\n\n"
    "Exemplo:\n"
    "  racket flex.rkt 3.69 2.99\n\n"
  ))
))

(define flex (lambda (gasolina alcool)
  (define combustivel (if (>= (/ (* alcool 100) gasolina) 70) "gasolina" "álcool"))
  (display (string-append "Abasteça com " combustivel ".\n"))
))

(define is-numeric (lambda (string)
  (not (equal? #f (string->number string)))
))

(define (main args)
  (define gasolina (if (equal? 2 (length args)) (car  args) ""))
  (define alcool   (if (equal? 2 (length args)) (cadr args) ""))

  (if (and (is-numeric gasolina) (is-numeric alcool)) 
    (flex (string->number gasolina) (string->number alcool))
    (help)
  )
)

(main (vector->list (current-command-line-arguments)))
