#include <iostream>
#include <string>

using namespace std;

void help() {
	cout <<
		"Flex v1.0" << endl << endl <<
		"Uso:" << endl <<
		"  flex <preço da gasolina> <preço do álcool>" << endl << endl <<
		"Exemplo:" << endl <<
		"  flex 3.69 2.99" << endl << endl;
}

void flex(float gasolina, float alcool) {
	float perc = alcool * 100 / gasolina;
	string combustivel = (perc >= 70) ? "gasolina" : "álcool";
	cout << "Abasteça com " << combustivel << "." << endl;
}

bool is_numeric(const char * s) {
	if (s == NULL || *s == '\0') {
		return 0;
	}
	char * p;
	strtod (s, &p);
	return *p == '\0';
}

int main(int argc, char *argv[]) {
	if (argc == 3 && is_numeric(argv[1]) && is_numeric(argv[2])) {
		flex(stof(argv[1]), stof(argv[2]));
	} else {
		help();
	}
	return 0;
}
