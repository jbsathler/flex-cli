package main

import (
	"fmt"
	"os"
	"strconv"
)

func help() string {
	return "Flex v1.0\n\n" +
		"Uso:\n" +
		"  flex <preço da gasolina> <preço do alcool>\n\n" +
		"Exemplo:\n" +
		"  flex 3.69 2.99\n"
}

func flex(gasolina float64, alcool float64) string {
	combustivel := ""
	if perc := (alcool * 100 / gasolina); perc >= 70 {
		combustivel = "gasolina"
	} else {
		combustivel = "álcool"
	}
	return "Abasteça com " + combustivel + "."
}

func isNumeric(s string) bool {
    _, err := strconv.ParseFloat(s, 64)
    return err == nil
}

func main() {
	if (len(os.Args) == 3 && isNumeric(os.Args[1]) && isNumeric(os.Args[2])) {
		gasolina, _ := strconv.ParseFloat(os.Args[1], 64)
		alcool  , _ := strconv.ParseFloat(os.Args[2], 64)
		fmt.Println(flex(gasolina, alcool))
	} else {
		fmt.Println(help())
	}
}
