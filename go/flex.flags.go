package main

import (
	"fmt"
	"os"
	"flag"
)

func help() string {
	return "Flex v1.1\n\n" +
		"modo de uso:\n" +
		"  flex -g <preço da gasolina> -a <preço do alcool>\n\n" +
		"Exemplo:\n" +
		"  flex -g 3.69 -a 2.99\n"
}

func flex(gasolina float64, alcool float64) string {
	combustivel := ""
	if perc := (alcool * 100 / gasolina); perc >= 70 {
		combustivel = "gasolina"
	} else {
		combustivel = "álcool"
	}
	return "Abasteça com " + combustivel + "."
}

func main() {
	var gasolina float64
	var alcool   float64
	var helpFlag bool
	flag.Float64Var(&gasolina, "g", 0.00, "Preço da Gasolina")
	flag.Float64Var(&alcool  , "a", 0.00, "Preço do Álcool"  )
	flag.BoolVar(&helpFlag, "h", false, "Exibe a mensagem de ajuda")
	flag.Parse()
	if helpFlag {
		fmt.Println(help())
	} else if gasolina == 0.00 || alcool == 0.00 {
		fmt.Println(help())
		os.Exit(1)
	} else {
		fmt.Println(flex(gasolina, alcool))
	}
}
