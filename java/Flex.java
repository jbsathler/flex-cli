public class Flex {

	protected static String help() {
		return "Flex v1.0\n\n" +
			"Uso:\n" +
			"  java Flex <preço da gasolina> <preço do álcool>\n\n" +
			"Exemplo:\n" +
			"  java Flex 3.69 2.99\n";
	}

	protected static String flex (float gasolina, float alcool) {
		float perc = alcool * 100 / gasolina;
		String combustivel = (perc >= 70) ? "gasolina" : "álcool";
		return "Abasteça com " + combustivel + ".";
	}

	protected static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		if ((args.length == 2) && isNumeric(args[0]) && isNumeric(args[1])) {
			System.out.println(flex(Float.parseFloat(args[0]), Float.parseFloat(args[1])));
		} else {
			System.out.println(help());
		}
	}

}
