#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void help() {
	printf("Flex v1.0\n\n"
		"Uso:\n"
		"  flex <preço da gasolina> <preço do álcool>\n\n"
		"Exemplo:\n"
		"  flex 3.69 2.99\n\n"
	);
}

void flex(double gasolina, double alcool) {
	float perc = alcool * 100 / gasolina;
	char combustivel[8] = "";
	strcpy(combustivel, (perc >= 70 ? "gasolina" : "álcool"));
	printf("Abasteça com %s.\n", combustivel);
}

int is_numeric(const char * s) {
	if (s == NULL || *s == '\0') {
		return 0;
	}
	char * p;
	strtod (s, &p);
	return *p == '\0';
}

void main(int argc, char *argv[]) {
	if ((argc == 3) && is_numeric(argv[1]) && is_numeric(argv[2])) {
		flex(strtod(argv[1], NULL), strtod(argv[2], NULL));
	} else {
		help();
	}
}
