#!/bin/bash

function helpMsg {
	echo "Flex v1.0"
	echo ""
	echo "Uso:"
	echo "  flex.sh <preço da gasolina> <preço do álcool>"
	echo ""
	echo "Exemplo:"
	echo "  flex.sh 3.69 2.99"
	echo ""
}

function flex {
	GASOLINA=$1
	ALCOOL=$2
	PERC=`echo "$ALCOOL * 100 / $GASOLINA" | bc -l`
	PERC=${PERC%.*}
	if [ $PERC -ge 70 ]; then
		COMBUSTIVEL="gasolina"
	else
		COMBUSTIVEL="álcool"
	fi
	echo "Abasteça com $COMBUSTIVEL."
}

function is_numeric {
	VAR=$1
	if [[ $VAR =~ ^[-+]?([0-9]*\.[0-9]+|[0-9]+)$ ]]; then
		echo true
	else
		echo false
	fi
}

if [[ $# == 2 && $(is_numeric $1) == true && $(is_numeric $2) == true ]]; then
	flex $1 $2
else
	helpMsg
fi
