program Flex;

Uses SysUtils, StrUtils;

function help(): string;
begin
	help := 'Flex v1.0' + LineEnding + LineEnding +
		'Uso:' + LineEnding +
		'  flex <preço da gasolina> <preço do álcool>' + LineEnding + LineEnding +
		'Exemplo:' + LineEnding +
		'  flex 3.69 2.99' + LineEnding;
end;

function flex(gasolina, alcool: real): string;
var
	perc: real;
	combustivel: string;
begin
	perc := alcool * 100 / gasolina;
	combustivel := ifThen((perc >= 70), 'gasolina', 'álcool');
	flex := 'Abasteça com ' + combustivel + '.';
end;

function isNumeric(s: string): boolean;
var
	v: real;
	e: integer;
begin
	Val(s, v, e);
	isNumeric := (e = 0);
end;

begin
	if ((paramCount = 2) and (isNumeric(ParamStr(1)) and isNumeric(ParamStr(2)))) then
	begin
		writeln(flex(StrToFloat(ParamStr(1)), StrToFloat(ParamStr(2))));
	end
	else
		writeln(help());
end.
