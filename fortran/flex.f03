program FlexPrg
implicit none

    real    :: gasolina, alcool
    logical :: all_args_are_real

    all_args_are_real = .true.
    if (command_argument_count() .eq. 2) then
        call get_arg_as_real(1, gasolina, all_args_are_real)
        call get_arg_as_real(2, alcool  , all_args_are_real)
        if (all_args_are_real) then
            call flex(gasolina, alcool)
        else
            call help()
        end if
    else
        call help()
    end if

contains
    subroutine help()
        Print *, "Flex v1.0"//char(10)//char(10)//&
        "Uso:"//char(10)//&
        "  flex <preço da gasolina> <preço do álcool>"//char(10)//char(10)//&
        "Exemplo:"//char(10)//&
        "  flex 3.69 2.99"//char(10)
    end subroutine help

    subroutine flex(gasolina, alcool)
        real         :: gasolina, alcool, perc
        character(8) :: combustivel
        perc        = alcool * 100 / gasolina
        combustivel = merge('gasolina', 'álcool ', perc >= 70)
        Print *, 'Abasteça com '//trim(combustivel)//'.'
    end subroutine flex

    subroutine get_arg_as_real(index, arg, is_real)
        integer, intent (in   ) :: index
        real   , intent (out  ) :: arg
        logical, intent (inout) :: is_real
        character(6)            :: string
        integer                 :: err

        call get_command_argument(index, string)
        Read(string, *, IOSTAT=err) arg
        is_real = (err == 0) .and. is_real
    end subroutine get_arg_as_real

end program FlexPrg
