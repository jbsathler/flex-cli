actor Main
  fun help(): String => 
    "Flex v1.0\n\n" +
    "Uso:\n" +
    "  Flex <preço da gasolina> <preço do álcool>\n\n" +
    "Exemplo:\n" +
    "  Flex 3.69 2.99\n"

  fun flex (gasolina: F64, alcool: F64): String =>
    var perc: F64 = (alcool * 100.0) / gasolina
    var combustivel: String = if (perc >= 70.0) then "gasolina" else "álcool" end
    "Abasteça com " + combustivel + "."

  new create(env: Env) =>
    try
      var gasolina = env.args(1)?.f64()?
      var alcool   = env.args(2)?.f64()?
      env.out.print(flex(gasolina, alcool))
    else
      env.out.print(help())
    end
