# Flex - CLI (Command-Line Interface)

Código-fonte da calculadora flex, edição de linha de comando, escrita em diversas linguagens.

----
## Linguagens contempladas:

### Interpretadas

01. PHP
02. JavaScript
03. CoffeeScript
04. TypeScript (+ Deno)
05. ShellScript
06. TCL
07. Dart
08. Lua
09. Ruby
10. Perl
11. Python
12. Elixir
13. SmallTalk
14. Common Lisp
15. Kotlin (+ KScript)
16. Julia
17. Groovy
18. R
19. Octave/MatLab
20. PowerShell
21. Batch
22. Euphoria
23. REXX
24. PureScript
25. Clojure
26. Hack (HHVM)
27. Elm
28. Scheme (Chicken, Racket)

### Compiladas

29. Go
30. C
31. C++
32. C#
33. Java
34. Pascal
35. Fortran
36. FreeBASIC
37. QuickBASIC (QB64)
38. Clipper
39. Cobol
40. Haskell
41. Rust
42. Sather (Sather-K)
43. Scala
44. Haxe
45. D
46. F#
47. Swift
48. Objective-C
49. Eiffel
50. Erlang
51. Crystal
52. Nim
53. V
54. Zig
55. Pony
56. OCaml
57. Red

----
## Características

* #### Mensagem de ajuda:

```
Flex v1.0

Uso:
  flex <preço da gasolina> <preço do álcool>

Exemplo:
  flex 3.69 2.99
```

* Chamar o interpretador antes dos scripts de linguagens interpretadas ou configurar o **_hash-bang_ (#!)** no início do script, caso o SO tenha suporte a este recurso.
* Este repositório **não armazena os binários executáveis pré-compilados**. Portanto, os códigos-fonte de linguagens compiladas **precisam ser compilados** na plataforma de destino antes de serem executados.
