#!/usr/bin/octave -qf
function r = flex(gasolina, alcool)
    #printf("gasolina: %d\nálcool: %d\n", gasolina, alcool);
    perc = (alcool * 100 / gasolina);
    combustivel = ifelse(perc >= 70, " gasolina", " álcool");
    r = strcat("Abasteça com", combustivel, ".\n");
endfunction

function r = help
    r = strcat("Flex v1.0\n\n",
        "Uso:\n",
        "  octave flex.m <preço da gasolina> <preço do álcool>\n\n",
        "Exemplo:\n",
        "  octave flex.m 3.69 2.99\n\n"
    );
endfunction

function r = is_numeric(a)
    if (isnumeric(a))
        r = 1;
    else
        o = str2num(a);
        r = !isempty(o);
    endif
endfunction

arg_list = argv();
if (length(arg_list) == 2 && is_numeric(arg_list{1}) && is_numeric(arg_list{2}))
    printf(flex(str2num(arg_list{1}), str2num(arg_list{2})));
else
    printf(help());
endif
