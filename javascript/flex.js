#!/usr/bin/env node
'use strict';

var help = function() {
	return "Flex v1.0\n\n" +
		"Uso:\n" +
		"  node flex.js <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  node flex.js 3.69 2.99\n";
};

var flex = function(gasolina, alcool) {
	var perc = alcool * 100 / gasolina;
	var combustivel = (perc >= 70) ? "gasolina" : "álcool";
	return "Abasteça com " + combustivel + ".";
};

var isNumeric = function(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

var args = process.argv.slice(2);
if (args.length == 2 && isNumeric(args[0]) && isNumeric(args[1])) {
	console.log(flex(args[0], args[1]));
} else {
	console.log(help());
}
