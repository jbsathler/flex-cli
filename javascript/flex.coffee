#!/usr/bin/env coffee
'use strict';

help = () ->
	return "Flex v1.0\n\n" +
		"Uso:\n" +
		"  coffee flex.coffee <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  coffee flex.coffee 3.69 2.99\n"

flex = (gasolina, alcool) ->
	perc = alcool * 100 / gasolina
	combustivel = if (perc >= 70) then "gasolina" else "álcool"
	return "Abasteça com " + combustivel + "."

isNumeric = (n) ->
	return !isNaN(parseFloat(n)) && isFinite(n)

args = process.argv.slice(2)
if ((args.length == 2) && isNumeric(args[0]) && isNumeric(args[1]))
	console.log(flex(args[0], args[1]))
else
	console.log(help())
