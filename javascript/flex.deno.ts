#!/usr/bin/env -S deno run
'use strict';

const help = () : string => {
  return "Flex v1.0\n\n" +
    "Uso:\n" +
    "  deno run flex.deno.ts <preço da gasolina> <preço do álcool>\n\n" +
    "Exemplo:\n" +
    "  deno run flex.deno.ts 3.69 2.99\n";
}

const flex = (gasolina: number, alcool: number): string => {
  const perc        : number = alcool * 100 / gasolina;
  const combustivel : number = (perc >= 70) ? "gasolina" : "álcool";
  return `Abasteça com ${combustivel}.`;
}

const isNumeric = (n: string): boolean => {
  return !isNaN(Number(n));
}

console.log(
  (Deno.args.length == 2 && isNumeric(Deno.args[0]) && isNumeric(Deno.args[1])) 
    ? flex(Number(Deno.args[0]), Number(Deno.args[1]))
    : help()
);
