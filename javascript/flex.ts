#!/usr/bin/env ts-node
/// <reference path="/home/jaderson/.npm-global/lib/node_modules/@types/node/index.d.ts" />
'use strict';

function help() {
	return "Flex v1.0\n\n" +
		"Uso:\n" +
		"  ts-node flex.ts <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  ts-node flex.ts 3.69 2.99\n";
};

function flex(gasolina: number, alcool: number) {
	let perc = alcool * 100 / gasolina;
	let combustivel = (perc >= 70) ? "gasolina" : "álcool";
	return "Abasteça com " + combustivel + ".";
};

function isNumeric(n: string) {
	return !isNaN(Number(n));
}

const args = process.argv.slice(2);
if (args.length == 2 && isNumeric(args[0]) && isNumeric(args[1])) {
	console.log(flex(Number(args[0]), Number(args[1])));
} else {
	console.log(help());
}
