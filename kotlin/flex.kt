#!/usr/bin/env kscript

fun help(): String =
	"Flex v1.0\n\n" +
	"Uso:\n" +
	"  flex.kt <preço da gasolina> <preço do álcool>\n\n" +
	"Exemplo:\n" +
	"  flex.kt 3.69 2.99\n"

fun flex (gasolina: Float, alcool: Float): String {
	val perc = (alcool * 100 / gasolina)
	val combustivel = if (perc >= 70) "gasolina" else "álcool"
	return "Abasteça com: " + combustivel + "."
}

fun isNumeric(input: String): Boolean = try {
	input.toDouble()
	true
} catch(e: NumberFormatException) {
	false
}

fun main(args: Array<String>) {
	if (args.size == 2 && isNumeric(args[0]) && isNumeric(args[1])) {
		println(flex(args[0].toFloat(), args[1].toFloat()))
	} else {
		println(help())
	}
}
