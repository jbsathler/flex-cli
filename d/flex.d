import std.stdio, std.string, std.array, std.conv;

void help() {
	writefln(
		"Flex v1.0\n\n"
		"Uso:\n"
		"  flex <preço da gasolina> <preço do álcool>\n\n"
		"Exemplo:\n"
		"  flex 3.69 2.99\n"
	);
}

void flex(float gasolina, float alcool) {
	float perc = alcool * 100 / gasolina;
	string combustivel = (perc >= 70) ? "gasolina" : "álcool";
	writefln("Abasteça com %s.", combustivel);
}

void main(in string[] args) {
	if (args.length == 3 && args[1].strip().isNumeric(true) && args[2].strip().isNumeric(true)) {
		flex(to!float(args[1]), to!float(args[2]));
	} else {
		help();
	}
}
