class Flex {
	static function help():String {
		return "Flex v1.0\n\n" +
			"Uso:\n" +
			"  flex <preço da gasolina> <preço do álcool>\n\n" +
			"Exemplo:\n" +
			"  flex 3.69 2.99\n";
	}

	static function flex(gasolina:Float, alcool:Float):String {
		var perc = alcool * 100 / gasolina;
		var combustivel = if (perc >= 70) "gasolina" else "álcool";
		return "Abasteça com " + combustivel + ".";
	};

	static function isNumeric(val:String):Bool {
		return Math.isFinite(Std.parseFloat(val));
	}

	static public function main():Void {
		var args = Sys.args();
		Sys.println(if (args.length == 2 && isNumeric(args[0]) && isNumeric(args[1])) {
			flex(Std.parseFloat(args[0]), Std.parseFloat(args[1]));
		} else {
			help();
		});
	}
}
