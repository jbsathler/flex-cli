#!/usr/bin/env Rscript

help <- function() {
	cat("Flex v1.0\n\n",
		"Uso:\n",
		"  Rscript flex.r <preço da gasolina> <preço do álcool>\n\n",
		"Exemplo:\n",
		"  Rscript flex.r 3.69 2.99\n\n"
	)
}

flex <- function(gasolina, alcool) {
	perc = alcool * 100 / gasolina
	cat("Abasteça com ", ifelse(perc >= 70, "gasolina", "álcool"), ".\n", sep="")
}

isNumeric <- function(str) {
	suppressWarnings(!is.na(as.numeric(str)))
}

args <- (commandArgs(TRUE))
if(length(args) == 2 && isNumeric(args[1]) && isNumeric(args[2])) {
	flex(as.numeric(args[1]), as.numeric(args[2]))
} else {
	help()
}
