#!/usr/bin/env perl

sub Help {
	print "Flex v1.0\n" .
		"Uso:\n" .
		"  flex.perl <preço da gasolina> <preço do álcool>\n\n" .
		"Exemplo:\n" .
		"  flex.perl 3.69 2.99\n\n";
}

sub Flex {
	$gasolina    = $_[0];
	$alcool      = $_[1];
	$perc        = ($alcool * 100 / $gasolina);
	$combustivel = ($perc >= 70) ? "gasolina" : "álcool";
	print "Abasteça com " . $combustivel . ".\n";
}

sub isNumeric {
	no warnings "numeric";
	$x = $_[0];
	return ($x + 0 ne $x) ? 0 : 1;
}

if ($#ARGV == 1 && isNumeric($ARGV[0]) && isNumeric($ARGV[1])) {
	Flex($ARGV[0], $ARGV[1]);
} else {
	Help();
}
