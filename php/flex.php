#!/usr/bin/env php
<?php

function help() {
	return "Flex v1.0\n\n" .
		"Uso:\n" .
		"  flex.php <preço da gasolina> <preço do álcool>\n\n" .
		"Exemplo:\n" .
		"  flex.php 3.69 2.99\n\n";
}

function flex ($gasolina, $alcool) {
	$perc = $alcool * 100 / $gasolina;
	return "Abasteça com " . ($perc >= 70 ? "gasolina" : "álcool") . ".\n";
}

if (count($argv) == 3 and is_numeric($argv[1]) and is_numeric($argv[2])) {
	echo flex($argv[1], $argv[2]);
} else {
	echo help();
}
