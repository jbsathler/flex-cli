module Main where

import Prelude
import Effect (Effect)
import Effect.Console (log)
import Node.Process (argv)
import Data.Array ((!!))
import Data.Number (fromString)
import Data.Maybe (Maybe, fromMaybe)

help :: String
help = "Flex v1.0\n\n" <>
  "Uso:\n" <>
  "  flex.sh <preço da gasolina> <preço do álcool>\n\n" <>
  "Exemplo:\n" <>
  "  flex.sh 3.69 2.99\n"

argToNumber :: Maybe String -> Number
argToNumber arg = do fromMaybe 0.0 (fromString (fromMaybe "0" arg))

calcPerc :: Number -> Number -> Number
calcPerc gasolina alcool = alcool * 100.0 / gasolina

combustivel :: Number -> String
combustivel perc = if perc >= 70.0 then "gasolina" else "álcool"

flex :: Number -> Number -> String
flex gasolina alcool = "Abasteça com " <> combustivel (calcPerc gasolina alcool) <> "."

main :: Effect Unit
main = do
  args <- argv
  let g = argToNumber (args !! 2)
  let a = argToNumber (args !! 3)
  if (g /= 0.0) && (a /= 0.0) then do
    log (flex g a)
  else
    log (help)
