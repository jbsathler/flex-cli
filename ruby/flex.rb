#!/usr/bin/env ruby

def help
	puts "Flex v1.0\n" +
		"Uso:\n" +
		"  flex.rb <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  flex.rb 3.69 2.99\n\n"
end

def flex(gasolina, alcool)
	perc = (alcool * 100 / gasolina)
	combustivel = perc >= 70 ? "gasolina" : "álcool"
	puts "Abasteça com #{combustivel}."
end

def is_numeric(str)
	return str.to_f.to_s == str
end

if (ARGV.length == 2) and is_numeric(ARGV[0]) and is_numeric(ARGV[1])
	gasolina, alcool = ARGV[0].to_f(), ARGV[1].to_f()
	flex(gasolina, alcool)
else
	help()
end
