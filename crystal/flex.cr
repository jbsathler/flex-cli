def help
	puts "Flex v1.0\n" +
		"Uso:\n" +
		"  flex <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  flex 3.69 2.99\n\n"
end

def flex(gasolina, alcool)
	perc = (alcool * 100 / gasolina)
	combustivel = perc >= 70 ? "gasolina" : "álcool"
	puts "Abasteça com #{combustivel}."
end

def is_numeric(str)
  begin
    return str == str.to_f.to_s
  rescue
    return false
  end
end

if (ARGV.size == 2) && is_numeric(ARGV[0]) && is_numeric(ARGV[1])
	gasolina, alcool = ARGV[0].to_f(), ARGV[1].to_f()
	flex(gasolina, alcool)
else
	help()
end
