const std = @import("std");

fn help() void {
  std.debug.warn("{}{}{}{}{}", .{
    "Flex v1.0\n\n",
    "Uso:\n",
    "  flex <preço da gasolina> <preço do álcool>\n\n",
    "Exemplo:\n",
    "  flex 3.69 2.99\n\n"
  });
}

fn flex(gasolina: f64, alcool: f64) void {
  const perc = alcool * 100 / gasolina;
  const combustivel = if (perc >= 70) "gasolina" else "álcool";
  std.debug.warn("Abasteça com {}.\n", .{combustivel});
}

fn isNumeric(s: []const u8) bool {
  const result = std.fmt.parseFloat(f64, s) catch | err | return false;
  return true;
}

fn strToFloat(s: []const u8) f64 {
  return std.fmt.parseFloat(f64, s) catch | err | return 0.00;
}

pub fn main() !void {
  const args = try std.process.argsAlloc(std.heap.page_allocator);
  defer std.process.argsFree(std.heap.page_allocator, args);

  if ((args.len == 3) and isNumeric(args[1]) and isNumeric(args[2])) {
    flex(strToFloat(args[1]), strToFloat(args[2]));
  } else {
    help();
  }
}
