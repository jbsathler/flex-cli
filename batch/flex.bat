@echo off
CLS
SETLOCAL enabledelayedexpansion
set /a c=1
set /a valid=1
:loop
if @%1==@ goto done
	set p%c%=%~1
	set /a c=c+1
	shift
	goto loop
:done
if %c%==3 (
	SET "var="&for /f "delims=0123456789" %%i in ("%p1%") do set var=%%i
	if not defined var (
		SET "var="&for /f "delims=0123456789" %%i in ("%p2%") do set var=%%i
		if not defined var (
			CALL :Flex %p1%, %p2%
		) else (
			CALL :Help
		)
	) else (
		CALL :Help
	)
) else (
	CALL :Help
)
EXIT /B %ERRORLEVEL%
:Help
echo Flex v1.0
echo.
echo Uso:
echo   flex.bat [pre�o da gasolina] [pre�o do �lcool]
echo.
echo Exemplo:
echo   flex.bat 369 299
echo.
echo (ATEN��O: informar os pre�os sem ponto decimal!)
echo.
EXIT /B 0
:Flex
set /a perc=%~2*100/%~1
if %perc% GEQ 70 (
	set combustivel=gasolina
) else (
	set combustivel=�lcool
)
echo Abaste�a com %combustivel%.
EXIT /B 0
