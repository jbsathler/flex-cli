import System.Environment

help = "Flex v1.0\n\n" ++
        "Uso:\n" ++
        "  flex <preço da gasolina> <preço do álcool>\n\n" ++
        "Exemplo:\n" ++
        "  flex 3.69 2.99\n"

flex :: Float -> Float -> String
flex gasolina alcool = do
    let perc = (alcool * 100 / gasolina)
    let combustivel = if perc >= 70 then "gasolina" else "álcool"
    "Abasteça com " ++ combustivel ++ "."

isNumeric s = case reads s :: [(Double, String)] of
    [(_, "")] -> True
    _         -> False

main :: IO ()
main = do
    argv <- getArgs
    if length argv == 2 && isNumeric(argv !! 0) && isNumeric(argv !! 1) then do
        let gasolina = read (argv !! 0) :: Float
        let alcool   = read (argv !! 1) :: Float
        putStrLn (flex gasolina alcool)
    else do
        putStrLn help
