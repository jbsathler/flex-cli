class
	FLEX

insert
	ARGUMENTS

create {ANY}
	make

feature {ANY}

	help: STRING
		do
			Result := "Flex v1.0%N%N" +
				"Uso:%N" +
				"  flex <preço da gasolina> <preço do álcool>%N%N" +
				"Exemplo:%N" +
				"  flex 3.69 2.99%N%N"
		end

	flex(gasolina: REAL; alcool: REAL): STRING
		local
			perc        : REAL
			combustivel : STRING
		do
			perc        := (alcool * 100.0 / gasolina)
			combustivel := if perc >= 70 then "gasolina" else "álcool" end
			Result      := "Abasteça com " + combustivel + ".%N"
		end

    make
		do
			if argument_count = 2 and then argument(1).is_real and then argument(2).is_real then
				print(flex(argument(1).to_real, argument(2).to_real))
			else
				print(help)
			end
		end

end
