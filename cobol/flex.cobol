IDENTIFICATION DIVISION.
PROGRAM-ID. FLEX.

DATA DIVISION.
   WORKING-STORAGE SECTION.
   01 arg            PIC X(50) VALUE SPACES.
   01 counter        PIC 9(1)  VALUE 0.
   01 gasolina       PIC 9(3)V9(2).
   01 alcool         PIC 9(3)V9(2).
   01 perc           PIC 9(3)V9(2).

PROCEDURE DIVISION.
   ACCEPT arg FROM ARGUMENT-VALUE
   PERFORM UNTIL arg = SPACES
      ADD 1 TO counter
      IF counter = 1 THEN
         MOVE arg TO gasolina
      ELSE
         MOVE arg TO alcool
      END-IF
      MOVE SPACES TO arg
      ACCEPT arg FROM ARGUMENT-VALUE
   END-PERFORM.

   A000-FIRST-PARA.
   IF counter = 2 THEN
      COMPUTE perc = (alcool * 100 / gasolina)
      IF perc >= 70 THEN
         DISPLAY "Abasteça com gasolina."
      ELSE
         DISPLAY "Abasteça com alcool."
      END-IF
   ELSE
      DISPLAY 'Flex v1.0'
      DISPLAY ' '
      DISPLAY 'Uso:'
      DISPLAY '  flex <preço da gasolina> <preço do álcool>'
      DISPLAY ' '
      DISPLAY 'Exemplo:'
      DISPLAY '  flex 3.69 2.99'
      DISPLAY ' '
   END-IF.

STOP RUN.
