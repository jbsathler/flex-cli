#!/usr/bin/env hhvm
namespace HH;

function help(): string {
	return "Flex v1.0" . \PHP_EOL . \PHP_EOL .
		"Uso:" . \PHP_EOL .
		"  hhvm flex.hack <preço da gasolina> <preço do álcool>" . \PHP_EOL . \PHP_EOL .
		"Exemplo:" . \PHP_EOL .
		"  hhvm flex.hack 3.69 2.99" . \PHP_EOL . \PHP_EOL;
}

function flex (num $gasolina, num $alcool): string {
	$perc = $alcool * 100 / $gasolina;
	return "Abasteça com " . ($perc >= 70 ? "gasolina" : "álcool") . "." . \PHP_EOL;
}

function is_numeric(mixed $value): bool {
  return (float)$value != 0.0;
}

<<__EntryPoint>>
function main(): void {
  $argv = vec(global_get('argv') as Container<_>);
  if (Lib\C\count($argv) == 3 && is_numeric($argv[1]) && is_numeric($argv[2])) {
    echo flex((float)$argv[1], (float)$argv[2]);
  } else {
    echo help();
  }
}
