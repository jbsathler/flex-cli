import os

fn help() string {
    return "Flex v1.0\n\n" +
        "Uso:\n" +
        "  flex <preço da gasolina> <preço do alcool>\n\n" +
        "Exemplo:\n" +
        "  flex 3.69 2.99\n"
}

fn flex(gasolina f64, alcool f64) string {
    perc := (alcool * 100 / gasolina)
    combustivel := if perc >= 70 { "gasolina" } else { "álcool" }
    return "Abasteça com " + combustivel + "."
}

fn is_numeric(s string) bool {
    return s == s.f64().str()
}

fn main() {
    if os.args.len == 3 && is_numeric(os.args[1]) && is_numeric(os.args[2]) {
        gasolina, alcool := os.args[1].f64(), os.args[2].f64()
        println(flex(gasolina, alcool))
    } else {
        println(help())
    }
}
