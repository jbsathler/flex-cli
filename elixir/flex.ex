#!/usr/bin/env elixir

defmodule Flex do
	def help() do
		IO.puts(
			"Flex v1.0\n\n" <>
			"Uso:\n" <>
			"  flex.ex <preço da gasolina> <preço do álcool>\n\n" <>
			"Exemplo:\n" <>
			"  flex.ex 3.69 2.99\n"
		)
	end

	def flex(gasolina, alcool) do
		perc = (alcool * 100 / gasolina)
		combustivel = if perc >= 70 do "gasolina" else "álcool" end
		IO.puts("Abasteça com " <> combustivel <> ".")
	end

	def is_numeric(str) do
		case Float.parse(str) do
			{_num, ""} -> true
			_          -> false
		end
	end
end

if length(System.argv) == 2 do
	[gasolina, alcool] = System.argv
	if Flex.is_numeric(gasolina) && Flex.is_numeric(alcool) do
		{gasolina, _} = Float.parse(gasolina)
		{alcool  , _} = Float.parse(alcool  )
		Flex.flex(gasolina, alcool)
	else
		Flex.help()
	end
else
	Flex.help()
end
