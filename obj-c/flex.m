#import <Foundation/Foundation.h>

void NSPrint(NSString *format, ...) {
	va_list args;
	va_start(args, format);
	fputs([[[[NSString alloc] initWithFormat:format arguments:args] autorelease] UTF8String], stdout);
	va_end(args);
}

void help() {
	NSPrint(@"Flex v1.0\n\n"
		"Uso:\n"
		"  flex <preço da gasolina> <preço do álcool>\n\n"
		"Exemplo:\n"
		"  flex 3.69 2.99\n\n"
	);
}

void flex(float gasolina, float alcool) {
	float perc = alcool * 100 / gasolina;
	NSString *combustivel = @"";
	if (perc >= 70) {
		combustivel = @"gasolina";
	} else {
		combustivel = @"álcool";
	}
	NSPrint(@"Abasteça com %@.\n", combustivel);
}

bool isNumeric(NSString *s) {
	NSScanner *sc = [NSScanner scannerWithString: s];
	if ([sc scanFloat:NULL]) {
		return [sc isAtEnd];
	}
	return NO;
}

int main() {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSArray *args = [[NSProcessInfo processInfo] arguments];
	if ([args count] == 3 && isNumeric([args objectAtIndex:1]) && isNumeric([args objectAtIndex:2])) {
		flex([[args objectAtIndex:1] floatValue], [[args objectAtIndex:2] floatValue]);
	} else {
		help();
	}
	[pool drain];
	return 0;
}
