object Flex {
	def help(): String = {
		return "Flex v1.0\n\n" +
			"Uso:\n" +
			"  scala Flex <preço da gasolina> <preço do álcool>\n\n" +
			"Exemplo:\n" +
			"  scala Flex 3.69 2.99\n"
	}

	def flex (gasolina: Float, alcool: Float): String = {
		var perc:Float = (alcool * 100 / gasolina)
		var combustivel:String = if (perc >= 70) "gasolina" else "álcool"
		return "Abasteça com " + combustivel + "."
	}

	def isNumeric(str: String): Boolean = {
		try {
			str.toDouble; true
		} catch {
			case e: NumberFormatException => false
		}
	}

	def main(args: Array[String]) {
		if (args.size == 2 && isNumeric(args(0)) && isNumeric(args(1))) {
			println(flex(args(0).toFloat, args(1).toFloat))
		} else {
			println(help())
		}
	}
}
