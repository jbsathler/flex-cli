#!/usr/bin/env groovy

class Flex {

	static void help() {
		println(
			"Flex v1.0\n\n"+
			"Uso:\n" +
			"  groovy flex.groovy <preço da gasolina> <preço do álcool>\n\n" +
			"Exemplo:\n" +
			"  groovy flex.groovy 3.69 2.99\n"
		);
	}

	static void flex(float gasolina, float alcool) {
		float perc = alcool * 100 / gasolina;
		String combustivel = (perc >= 70) ? "gasolina" : "álcool";
		println("Abasteça com " + combustivel + ".");
	}

	static boolean isNumeric(String str) {
		try {
			double d = Double.parseDouble(str);
		} catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	static void main(String[] args) {
		if (args.length == 2 && isNumeric(args[0]) && isNumeric(args[1])) {
			flex(Float.parseFloat(args[0]), Float.parseFloat(args[1]));
		} else {
			help();
		}
	}
}
