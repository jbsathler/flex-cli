#!/usr/bin/env -S clojure -M

(defn help []
  (def helpMsg [
    "Flex v1.0"
    ""
    "Uso:"
    "  clojure -M flex.clj <preço da gasolina> <preço do álcool>"
    ""
    "Exemplo:"
    "  clojure -M flex.clj 3.69 2.99"
    ""
  ])
  (mapv println helpMsg)
)

(defn perc [gasolina alcool] (/ (* alcool 100) gasolina))

(defn combustivel [perc] (if (>= perc 70) "gasolina" "álcool"))

(defn flex [gasolina alcool] (apply str ["Abasteça com " (combustivel (perc gasolina alcool)) "."]))

(defn is-double [arg] (and (not= nil arg) (not= nil (parse-double arg))))

(let [[gasolina alcool] *command-line-args*]
  (if (and (is-double gasolina) (is-double alcool))
    (println (flex (read-string gasolina) (read-string alcool)))
    (help)
  )
)
