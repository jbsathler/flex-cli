use std::env;

fn help() {
	println!(concat!(
		"Flex v1.0\n\n",
		"Uso:\n",
		"  flex <preço da gasolina> <preço do álcool>\n\n",
		"Exemplo:\n",
		"  flex 3.69 2.99\n"
	));
}

fn flex(gasolina:f32, alcool:f32) {
	let perc:f32 = alcool * 100f32 / gasolina;
	let combustivel = if perc >= 70f32 {"gasolina"} else {"álcool"};
	println!("Abasteça com {}.", combustivel);
}

fn main() {
	let args: Vec<_> = env::args().collect();
	if args.len() == 3 && args[1].parse::<f32>().is_ok() && args[2].parse::<f32>().is_ok() {
		flex(args[1].parse::<f32>().unwrap(), args[2].parse::<f32>().unwrap());
	} else {
		help();
	}
}
