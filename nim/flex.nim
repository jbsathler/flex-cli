import os, strutils

proc helpMsg(): string =
    result =
        "Flex v1.0\n\n" &
        "modo de uso:\n" &
        "  flex <preço da gasolina> <preço do álcool>\n\n" &
        "Exemplo:\n" &
        "  flex 3.69 2.99\n"

proc flex(gasolina: float, alcool: float): string =
    var perc = (alcool * 100 / gasolina)
    var combustivel = if perc >= 70: "gasolina" else: "álcool"
    result = "Abasteça com " & combustivel & "."

proc isFloat(str: string): bool =
    try:
        var _ = parseFloat(str)
        result = true
    except ValueError:
        result = false

if paramCount() == 2 and isFloat(paramStr(1)) and isFloat(paramStr(2)):
    echo flex(parseFloat(paramStr(1)), parseFloat(paramStr(2)))
else:
    echo helpMsg()
