#!/usr/bin/env eui

include get.e
include std/utils.e
include std/convert.e

function help()
	puts(1, "Flex v1.0\n\n" &
		"Uso:\n" &
		"  flex.e <preço da gasolina> <preço do álcool>\n\n" &
		"Exemplo:\n" &
		"  flex.e 3.69 2.99\n\n"
	)
	return 1
end function

function flex(atom gasolina, atom alcool)
	atom perc = alcool * 100 / gasolina
	sequence combustivel = iif(perc >= 70, "gasolina", "álcool")
	puts(1, "Abasteça com " & combustivel & ".\n")
	return 1
end function

function is_numeric(sequence s)
    sequence val
    val = value(s)
    return val[1]=GET_SUCCESS and atom(val[2])
end function

constant cmd = command_line()
if length(cmd)=4 and is_numeric(cmd[3]) and is_numeric(cmd[4]) then
	flex(to_number(cmd[3]), to_number(cmd[4]))
else
	help()
end if
