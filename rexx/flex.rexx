#!/usr/bin/env rexx
PARSE arg args

if words(args) == 2 & datatype(word(args, 1), 'n') & datatype(word(args, 2), 'n') then
	say flex(word(args, 1), word(args, 2))
else
	say help()
exit

help:
	say "Flex v1.0"
	say ""
	say "Uso:"
	say "  flex.rexx <preço da gasolina> <preço do álcool>"
	say ""
	say "Exemplo:"
	say "  flex.rexx 3.69 2.99"
	return ""

flex:
	PARSE ARG gasolina, alcool
	perc = alcool * 100 / gasolina
	if perc >= 70 then
		combustivel = "gasolina"
	else
		combustivel = "álcool"
	return "Abasteça com "combustivel"."
