let help = "Flex v1.0\n\n" ^
  "Uso:\n" ^
  "  flex <preço da gasolina> <preço do álcool>\n\n" ^
  "Exemplo:\n" ^
  "  flex 3.69 2.99\n";;

let calcPerc gasolina alcool = alcool *. 100.0 /. gasolina;;

let combustivel perc = if perc >= 70.0 then "gasolina" else "álcool";;

let flex gasolina alcool = "Abasteça com " ^ combustivel (calcPerc gasolina alcool) ^ ".";;

let isFloat stringArg = 
  try
    let floatArg = float_of_string stringArg in
    floatArg >= 0.0 || floatArg < 0.0
  with
    _ -> false
  ;;

if Array.length Sys.argv == 3 && isFloat Sys.argv.(1) && isFloat Sys.argv.(2) then begin
  let g = float_of_string Sys.argv.(1) in
  let a = float_of_string Sys.argv.(2) in
  print_endline (flex g a)
end else begin
  print_endline help
end;;
