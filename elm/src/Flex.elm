module Flex exposing (program)

import Dict exposing (Dict)
import Posix.IO as IO exposing (IO, Process)
import Posix.IO.File as File
import Posix.IO.Process as Proc

help : String
help =
  "Flex v1.0\n\n" ++
  "Uso:\n" ++
  "  elm-cli run flex.elm <preço da gasolina> <preço do álcool>\n\n" ++
  "Exemplo:\n" ++
  "  elm-cli run flex.elm 3.69 2.99\n"

calcPerc : Float -> Float -> Float
calcPerc gasolina alcool = alcool * 100.0 / gasolina

combustivel : Float -> String
combustivel perc = if perc >= 70.0 then "gasolina" else "álcool"

flex : Float -> Float -> String
flex gasolina alcool = "Abasteça com " ++ (combustivel (calcPerc gasolina alcool)) ++ "."

parseFloat : String -> Float
parseFloat value = Maybe.withDefault 0.0 (String.toFloat(value))

program : Process -> IO ()
program process =
  Proc.print (
    case process.argv of
      [ _, gasolina, alcool ] ->
        if (String.toFloat(gasolina) /= Nothing && String.toFloat(alcool) /= Nothing) then
          (flex (parseFloat(gasolina)) (parseFloat(alcool)))
        else
          help
      _ ->
        help
  )
