func help() {
	print(
		"Flex v1.0\n\n" +
		"Uso:\n" +
		"  flex <preço da gasolina> <preço do álcool>\n\n" +
		"Exemplo:\n" +
		"  flex 3.69 2.99\n"
	)
}

func flex(gasolina: Double, alcool: Double) {
	let perc        = (alcool * 100 / gasolina)
	let combustivel = (perc >= 70) ? "gasolina" : "álcool"
	print("Abasteça com \(combustivel).")
}

func isNumeric(a: String) -> Bool {
	return Double(a) != nil
}

func unwrapDouble(a: String?) -> Double {
	if let num = Double(a!) {
		return num
	} else {
		return 0.0
	}
}

if (CommandLine.arguments.count == 3 && isNumeric(a: CommandLine.arguments[1]) && isNumeric(a: CommandLine.arguments[2])) {
	let gasolina = unwrapDouble(a:CommandLine.arguments[1])
	let alcool   = unwrapDouble(a:CommandLine.arguments[2])
	flex(gasolina:gasolina, alcool:alcool)
} else {
	help()
}
