#! /usr/bin/gst -f
help := 'Flex v1.0

Uso:
  gst -f flex.st <preço da gasolina> <preço do álcool>

Exemplo:
  gst -f flex.st 3.69 2.99
'

((Smalltalk getArgc == 2) and: [
((Smalltalk getArgv: 1) isNumeric) and: [
((Smalltalk getArgv: 2) isNumeric)
]])
	ifTrue: [
		gasolina    := (Smalltalk getArgv: 1) asNumber.
		alcool      := (Smalltalk getArgv: 2) asNumber.
		perc        := (alcool * 100 / gasolina).
		combustivel := (perc >= 70) ifTrue:['gasolina'] ifFalse:['álcool'].
		('Abasteça com ', combustivel, '.') displayNl
	]
	ifFalse: [
		(help) displayNl
	]
